﻿using System;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace AppealsFunction.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppealsController : ControllerBase
    {
        readonly List<KeyValuePair<string, StringValues>> CORSHeaders = new List<KeyValuePair<string, StringValues>>
        {
            new KeyValuePair<string, StringValues> ("Access-Control-Allow-Origin", new StringValues("*")),
            new KeyValuePair<string, StringValues> ("Access-Control-Allow-Headers", new StringValues("Content-Type")),
            new KeyValuePair<string, StringValues> ("Access-Control-Allow-Credentials", new StringValues("True"))
        };

        [HttpGet]
        public ActionResult<List<Appeal>> Get()
        {
            AddCORSHeaders();

            //TODO add error handling
            return SqlClient.GetAllAppeals();
        }

        [HttpGet("{status}")]
        public ActionResult<List<Appeal>> Get(Status status)
        {
            AddCORSHeaders();

            //TODO add error handling
            return SqlClient.GetAppealsByStatusSortDescByDateBy(status);
        }

        [HttpPost]
        public void Post([FromBody] Appeal appeal)
        {
            AddCORSHeaders();

            if (!ValidationUtil.ValidateAppeal(appeal))
            {
                //TODO add more info about validation
                HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
            }
            else
            {
                appeal.Status = Status.PENDING;
                appeal.DateSubmitted = DateTime.UtcNow;

                //TODO add error handling
                SqlClient.AddAppeal(appeal);
            }
        }

        [HttpPost("{id}")]
        public void Post(int id)
        {
            AddCORSHeaders();

            //TODO add error handling
            SqlClient.ChangeAppealStatus(id, Status.RESOLVED);
        }

        [HttpDelete]
        public void Delete()
        {
            AddCORSHeaders();

            HttpContext.Response.StatusCode = StatusCodes.Status405MethodNotAllowed;
        }

        [HttpOptions]
        public void Options()
        {
            AddCORSHeaders();
        }

        void AddCORSHeaders()
        {
            foreach (var header in CORSHeaders)
            {
                HttpContext.Response.Headers.Add(header);
            }
        }
    }
}