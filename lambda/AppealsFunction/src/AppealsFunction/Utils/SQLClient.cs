﻿using System.Linq;
using System.Collections.Generic;

using MySql.Data.MySqlClient;

using Dapper;

namespace AppealsFunction
{
    public static class SqlClient
    {
        private static string connectionString = "";

        public static List<Appeal> GetAllAppeals()
        {
            using (var conn = new MySqlConnection(connectionString))
            {
                var sql = "SELECT * FROM appeals";
                var result = conn.Query<Appeal>(sql).ToList();

                return result;
            }
        }

        public static List<Appeal> GetAppealsByStatus(Status status)
        {
            using (var conn = new MySqlConnection(connectionString))
            {
                var sql = "SELECT * FROM appeals WHERE status = @status";
                var result = conn.Query<Appeal>(sql, new { status }).ToList();

                return result;
            }
        }

        public static List<Appeal> GetAppealsByStatusSortDescByDateBy(Status status)
        {
            List<Appeal> appeals = GetAppealsByStatus(status);

            return appeals.OrderByDescending((a) => a.DateBy).ToList();
        }

        public static Appeal GetAppealById(int id)
        {
            using (var conn = new MySqlConnection(connectionString))
            {
                var sql = "SELECT * FROM appeals WHERE id = @id";
                var result = conn.Query<Appeal>(sql, new { id }).FirstOrDefault();

                return result;
            }
        }

        public static void AddAppeal(Appeal appeal)
        {
            //FIXME should not return void
            using (var conn = new MySqlConnection(connectionString))
            {
                var sql = "INSERT INTO appeals (description, datesubmitted, dateby, status) VALUES (@description, @datesubmitted, @dateby, @status)";
                conn.Execute(sql, new { appeal.Description, appeal.DateSubmitted, appeal.DateBy, appeal.Status });
            }
        }

        public static void ChangeAppealStatus(int id, Status status)
        {
            //FIXME should not return void
            using (var conn = new MySqlConnection(connectionString))
            {
                var sql = "UPDATE appeals SET status = @status WHERE id = @id";
                conn.Execute(sql, new { status, id });
            }
        }
    }
}
